# JWT

PHP JSON Web Token Library

* Current implementation supports RS256 only.

## Library Requirements
* PHP v5.3.3+

#### Class Object Parameters
* token
    * encoded token
* iss
    * Ex: jross.org
* aud 
    * Ex: jross.org_shortlink
* jti 
    * unique ID for the token, generated when signing
* publicKey
    * public key used to verify signatures
* privateKey
    * private key used to create signatures
* tokenLifetime
    * UNIX minutes that a signed token will be valid after being issued 
* header
    * object containing header elements
* payload
    * object containing payload elements
 
#### Functions
* urlSafeB64Decode(string)
    * URL-safe Base 64 Decode a string
* urlSafeB64Encode(string)
    * URL-safe Base 64 Encode a string
* verify()
    * For verifying the signature of token.  Return boolean.
* sign()
    * Assemble the token from header and payload objects, then sign using privateKey.  Save encoded token as token parameter.  Return boolean if we can validate the JWT we created.
* gen_uuid()
    * Used internally to generate a gen4 uuid for the jti

## Examples

#### Verifying a Token
* Set publicKey to string containing the public key part of the symmetric key pair
* Set the iss and aud parameters to what is expected
* Set the token parameter to the full encoded token to be verified
* Call verify().  If token is valid, returns true and populates header and payload objects

#### Creating a Token
* Set publicKey to string containing the public key part of the symmetric key pair
* Set privateKey to string containing the private key part of the symmetric key pair
* Set the iss and aud to what is desired in the JWT header
* Set the payload object to be encoded
* Call sign().  If token is created and validated, returns true and populates token parameter with the full, signed token
