<?php
/*
* webdev/jross-jwt-php/JWT.php
* Copyright (c) 2015 Jonathan E. Ross
*/

class JWT {

  public $token;
  public $iss;
  public $aud;
  public $jti;
  public $publicKey;
  public $privateKey;
  public $tokenLifetime;
  public $header;
  public $payload;
  
  public function __construct(){
    $this->token         = false;
    $this->iss           = false;
    $this->aud           = false;
    $this->jti           = false;
    $this->publicKey     = false;
    $this->privateKey    = false;
    $this->tokenLifetime = 0;
    $this->header        = false;
    $this->payload       = false;
  }

  public function verify(){
    if(!$this->iss || !$this->publicKey || !$this->publicKey || !$this->aud){
      error_log("JWT::verify() object not initialized");
      return false;
    }
    if(!$this->token){
      error_log("JWT::verify() token is empty");
      return false;
    }

    list($tokenHeader, $tokenPayload, $tokenSignature) = explode('.', $this->token);
    
    $jsonHeader   = JWT::urlSafeB64Decode($tokenHeader);
    $this->header = json_decode($jsonHeader);
    
    $jsonPayload   = JWT::urlSafeB64Decode($tokenPayload);
    $this->payload = json_decode($jsonPayload);

    $decodedSignature = JWT::urlSafeB64Decode($tokenSignature);
 
    $verify_input   = implode('.', array($tokenHeader,$tokenPayload));
    $validSignature = openssl_verify($verify_input, $decodedSignature, $this->publicKey, 'SHA256');
    $validAlg       = ($this->header->alg  == 'RS256')    ? true : false;  // Only accept tokens signed with our expected algorithm.
    $validIssuer    = ($this->payload->iss == $this->iss) ? true : false;  // Only accept tokens signed by our expected issuer.
    $validAudience  = ($this->payload->aud == $this->aud) ? true : false;  // Only accept tokens meant for us (audience).
    $notExpired     = ($this->payload->exp >= time())     ? true : false;  // Make sure the token has not yet expired.
    $notFuture      = ($this->payload->iat <= time())     ? true : false;  // Make sure the token is not from the future.

    $this->jti = $this->payload->jti;  // Extract the token ID from the payload

    if(   $validSignature 
       && $validAlg 
       && $validIssuer 
       && $validAudience 
       && $notExpired
       && $notFuture){
      return true;
    }
    else{
      if(!$validSignature) error_log("JWT::verify() bad signature");
      if(!$validAlg)       error_log("JWT::verify() bad algorithm");
      if(!$validIssuer)    error_log("JWT::verify() bad issuer");
      if(!$validAudience)  error_log("JWT::verify() bad audience");
      if(!$notExpired)     error_log("JWT::verify() token expired");
      if(!$notFuture)      error_log("JWT::verify() token issued in the future");
      return false;
    }
  }

  public function sign(){
    if(!$this->iss || !$this->publicKey || !$this->publicKey || !$this->aud){
      error_log("JWT::sign() object not initialized");
      return false;
    }

    // prepare the header and payload
    $this->jti     = JWT::gen_uuid();
    $this->header  = array('typ' => 'JWT', 'alg' => 'RS256');
    $this->uuid    = $this->payload['user']->uuid;
    $this->iat     = time();
    $this->exp     = time()+$this->tokenLifetime;
    $payloadHeader = array('iss' => $this->iss, 'aud' => $this->aud, 'jti' => $this->jti, 'exp' => $this->exp, 'iat' => $this->iat);
    $this->payload = array_merge($payloadHeader, $this->payload);

    $encodedHeader  = JWT::urlSafeB64Encode(json_encode($this->header));
    $encodedPayload = JWT::urlSafeB64Encode(json_encode($this->payload));
    
    $signature = '';
    $signature_input = implode('.', array($encodedHeader,$encodedPayload));
    if(openssl_sign($signature_input, $signature, $this->privateKey, 'SHA256')){
      $encodedSignature = JWT::urlSafeB64Encode($signature);
      $this->token = implode('.', array($signature_input,$encodedSignature)); 
      return $this->verify();  // Verify the JWT we just created
    } else{
      error_log("JWT::sign() openssl_sign failed");
      return false;
    }
  }

  private static function urlSafeB64Decode($input){
    $remainder = strlen($input) % 4;
      if ($remainder) {
        $padlen = 4 - $remainder;
        $input .= str_repeat('=', $padlen);
      }
      return base64_decode(strtr($input, '-_', '+/'));
  }

  private static function urlSafeB64Encode($input){
    return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
  }

  private static function gen_uuid(){
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
  }

}

?>
